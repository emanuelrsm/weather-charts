import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  readonly url = `https://cors-anywhere.herokuapp.com/https://samples.openweathermap.org/data/2.5/forecast/daily?id=524901&appid=b1b15e88fa797225412429c1c50c122a1`
  weather$: Observable<any>;

  constructor(private http: HttpClient) { }

  getWeatherData() {
    this.weather$ = this.http.get<any>(`${this.url}`).pipe(
      map(res => res)
    );
    return this.weather$;
    /* console.warn(this.weather$) */
  }
}
