import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../services/weather.service';
import { Chart } from 'chart.js'
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss']
})
export class ChartsComponent implements OnInit {
  weather$: any;
  weatherDates = [];
  max_temp: number;
  min_temp: number;
  weatherSubscription: Subscription;

  //For Charts
  type: string;
  chart = Chart;

  constructor(private weatherservice: WeatherService) {
    this.weatherSubscription = this.weatherservice.getWeatherData()
      .subscribe(
        res => {
          let weatherDates = [];

          let max_temp = res['list'].map(res => Math.round((res.temp.max) - 273.15));
          let min_temp = res['list'].map(res => Math.round((res.temp.min) - 273.15));
          let alldates = res['list'].map(res => res.dt)

          alldates.forEach(element => {
            let date = new Date(element * 1000)
            weatherDates.push(date
              .toLocaleTimeString('en', {
                month: 'short',
                day: 'numeric'
              })
            )
          });
          weatherDates = weatherDates.map(e => e.split(',').slice(0, 1));
          this.weatherDates = weatherDates;
          this.max_temp = max_temp;
          this.min_temp = min_temp;
        });
  }

  ngOnInit() {
    this.weather$ = this.weatherservice.getWeatherData();
  }

  infoPanel() {
    return this.weather$;
  }

  infoTable() {
    this.type = 'info';
    this.draw();
  }

  selectPieChart() {
    this.type = 'pie';
    this.draw();
  }

  selectBarChart() {
    this.type = 'bar';
    this.draw();
  }

  selectLineChart() {
    this.type = 'line';
    this.draw();
  }

  selectPolarChart() {
    this.type = 'polar';
    this.draw();
  }

  selectAllCharts() {
    this.type = 'all';
    this.draw();
  }

  draw() {

    // If chart is present, destroy previous one to avoid memory leaks
    if (this.chart.id >= 0) {
      this.chart.destroy();
    }

    //switch case
    switch (this.type) {
      case 'info':
        this.infoPanel();
        break;
      case 'pie':
        this.drawPieChart();
        break;
      case 'bar':
        this.drawBarChart();
        break;
      case 'line':
        this.drawLineChart();
        break;
      case 'polar':
        this.drawPolarChart();
        break;
      case 'all':
        this.drawAllCharts();
        break;
      default:
        break;
    }
  }

  drawPieChart() {
    this.chart = new Chart('pie', {
      type: 'pie',
      data: {
        labels: this.weatherDates,
        datasets: [
          {
            label: "Max Temps",
            backgroundColor: [
              'rgba(255, 99, 32, 0.6)',
              'rgba(50, 220, 135, 0.6)',
              'rgba(68, 17, 180, 0.6)',
              'rgba(55, 50, 50, 0.6)',
              'rgba(120, 63, 12, 0.6)',
              'rgba(180, 194, 110, 0.6)',
              'rgba(87, 160, 200, 0.6)',
              'rgba(200, 9, 180, 0.6)',
              'rgba(30, 200, 220, 0.6)'
            ],
            data: this.max_temp
          }
        ]
      },
      options: {
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: false,
            }
          ],
          yAxes: [
            {
              display: false,
            }
          ]
        }
      }
    })
  }

  drawBarChart() {
    this.chart = new Chart('bar', {
      type: 'bar',
      data: {
        labels: this.weatherDates,
        datasets: [
          {
            data: this.max_temp,
            label: 'Max Temp',
            backgroundColor: '#ff6384',
            fill: false
          },
          {
            data: this.min_temp,
            label: 'Min Temp',
            backgroundColor: '#36a2eb',
            fill: false
          }
        ]
      },
      options: {
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
            }
          ],
          yAxes: [
            {
              display: true,
            }
          ]
        }
      }
    })
  }

  drawLineChart() {
    this.chart = new Chart('line', {
      type: 'line',
      data: {
        labels: this.weatherDates,
        datasets: [
          {
            data: this.max_temp,
            label: 'Max Temp',
            borderColor: '#ff6384',
            fill: false
          },
          {
            data: this.min_temp,
            label: 'Min Temp',
            borderColor: '#36a2eb',
            fill: false
          }
        ]
      },
      options: {
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
            }
          ],
          yAxes: [
            {
              display: true,
              type: 'linear',
              ticks: {
                min: -20,
                max: 20
              }
            }
          ]
        }
      }
    })
  }

  drawPolarChart() {
    this.chart = new Chart('polar', {
      type: 'polarArea',
      data: {
        labels: this.weatherDates,
        datasets: [
          {
            data: this.max_temp,
            label: 'Max Temp',
            backgroundColor: [
              'rgba(255, 99, 32, 0.4)',
              'rgba(50, 220, 135, 0.4)',
              'rgba(68, 17, 180, 0.4)',
              'rgba(55, 50, 50, 0.4)',
              'rgba(120, 63, 12, 0.4)',
              'rgba(180, 194, 110, 0.4)',
              'rgba(87, 160, 200, 0.4)',
              'rgba(200, 9, 180, 0.4)',
              'rgba(30, 200, 220, 0.4)'
            ],
            fill: false
          },
          {
            data: this.min_temp,
            label: 'Min Temp',
            backgroundColor: [
              'rgba(255, 99, 32, 0.8)',
              'rgba(50, 220, 135, 0.8)',
              'rgba(68, 17, 180, 0.8)',
              'rgba(55, 50, 50, 0.8)',
              'rgba(120, 63, 12, 0.8)',
              'rgba(180, 194, 110, 0.8)',
              'rgba(87, 160, 200, 0.8)',
              'rgba(200, 9, 180, 0.8)',
              'rgba(30, 200, 220, 0.8)'
            ],
            fill: false
          }
        ]
      },
      options: {
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: false,
            }
          ],
          yAxes: [
            {
              display: false,
            }
          ]
        }
      }
    })
  }

  drawAllCharts() {
    // TODO: Draw everything latter
    console.warn(`draw everything`)
  }
}
